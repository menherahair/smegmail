#!/usr/bin/env -S -i bash
[[ "${PWD##*/}" == "tests" ]] || echo 'run tests in the tests dir' exit 69
[[ -d /usr/pkg/bin ]] && PATH="/usr/pkg/bin:${PATH}"

diff -y ./mail-encrypted-attachments <( gawk -v b=smegma -v gpg=cat -f ../bin/smegmail < ./mail-plain-attachments ) ||
echo "$0: output differs" >&2 && exit 1

# this one's tricky
# it looks wrong when using `cat` for encryption but that's what the content headers should look like

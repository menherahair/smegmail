#!/usr/bin/env -S -i bash
[[ "${PWD##*/}" == "tests" ]] || echo 'run tests in the tests dir' exit 69
[[ -d /usr/pkg/bin ]] && PATH="/usr/pkg/bin:${PATH}"

output=$( gawk -v gpg="smegma$RANDOM" -f ../bin/smegmail < ./mail-plain )
e="${?}"

if [[ $output != "" ]]
then ( echo "$0: output not empty" ; (( x++ )) ); fi

if (( e == 0 ))
then ( echo "$0: exit code not non-zero" ; (( x++ )) ) ; fi

exit $x

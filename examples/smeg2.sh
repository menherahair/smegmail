#!/bin/sh
# smeg2.sh - same as smeg.sh, but won't encrypt incoming pgp/mime messages
# to quote:
# # smeg.sh - smegmail wrapper that could work in .forward on sdf as-is
# # things to note regardless:
# # - this uses the old `gpg`, not `gpg2` executable
# # - this has no idea about your $GNUPG_HOME
# # - the gpg will try to encrypt to your LOGNAME, make sure it works
# # - this expects ~/bin/smegmail to be smegmail

# the .forward's environment is damn barren, best I could do in posix
# sh to reliably fix it is this
export HOME="${PWD}"
export LOGNAME="${HOME##*/}"
export PATH="${PATH}:/usr/pkg/bin/"

# store message
message="$(/bin/cat -)"

if
	# do headers say this message is encrypted?
		awk '/^$/ { exit 1 };
		     /protocol="application\/pgp-encrypted"/ { exit 0 }' \
		<<- _EOF
		${message}
		_EOF
then
	/bin/cat <<- _EOF
	"${message}"
	_EOF
else
	# call smegmail, encrypt to LOGNAME
		gawk \
			-v pgp='gpg -qaer $LOGNAME' \
			-f "${HOME}/bin/smegmail" \
		<<- _EOF
		${message}
		_EOF

fi >> "/var/mail/${LOGNAME}" # append to the system spoolfile

# usage examples

[TOC]

## invocation

you can feed the mail to smegmail on stdin or pass it as argument, but I
wouldn't count on it in noninteractive use:

    smegmail < mail

This will work fine if the shebang line works (it's set for use on netbsd by
default, you may need to adjust it) and your gnupg is configured with default
recipients.

passing options straight to `gawk` is the preferred invocation:

    gawk -v pgp='gpg2 -qaer morus@sdf.org' -f ~/bin/smegmail < mail

you can also use environment variables to same effect:

    env SMEG_PGP='gpg2 -qaer morus@sdf.org' gawk -f ~/bin/smegmail < mail

with working shebang:

    env SMEG_PGP='gpg2 -qaer morus@sdf.org' ~/bin/smegmail < mail

smegmail will spew the encrypted mail on stdout, then you can process it further
(eg by piping to `procmail`) or simply append it to your mailbox.

see also:

-   [gawk user manual, command line
    options](https://www.gnu.org/software/gawk/manual/gawk.html#Options)
-   [netbsd's env(1)
    manpage](https://man.freebsd.org/cgi/man.cgi?query=env&sektion=1&manpath=NetBSD+9.3)

## sdf

see [smeg.sh](examples/smeg.sh)

For use on sdf, all incoming mail may be encrypted using the \~/.forward file.
Due to it's limitations, it's by far the easiest to create a wrapper script and
make sure the correct environment is set there:

    #!/bin/sh
    # example smegmail wrapper
    export HOME="/sdf/udd/m/morus"
    export PATH="/usr/pkg/bin/:${PATH}"
    gawk \
      -v pgp='gpg2 --homedir ${HOME}/.config/gnupg -qaer morus' \
      -f "${HOME}/bin/smegmail" >> "${HOME}/mail/Inbox"

invoked in the .forward file using the full path like so:

    # ~/.forward
    "| /sdf/udd/m/morus/bin/smeg.sh"

or relying on PWD being your home directory - as it should be - one can simply
do:

    # ~/.forward
    "| bin/smeg.sh"

If the wrapper is executable (`chmod +x` it) and `gpg` can find the recipient's
public key, all incoming mail should be encrypted and appended to \~/mail/Inbox,
reachable by local mail clients as well as Sqirrelmail and IMAP. If the wrapper,
gawk, MTA or `gpg` fails somewhere, non-zero exit code will cause sdf's mailer
to return the email to the sender.

### skipping already encrypted mail

see [smeg2.sh](examples/smeg2.sh)

if smegmail is given a valid PGP/MIME message, it'll nest it inside it's own
PGP/MIME structure. This is largely harmeless (at least `mutt` has no trouble
opening these), but rather silly given good PGP practices are kept.

Detecting and not processing incoming PGP/MIME mail is outside the scope of
smegmail, but can be ~~easily~~ done inside the wrapper:

    #!/bin/sh
    # example smegmail wrapper, passing incoming PGP/MIME as-is
    export HOME="/sdf/udd/m/morus"
    export LOGNAME="${HOME##*/}"
    export PATH="/usr/pkg/bin/:${PATH}"

    message="$(cat -)"

    if
      printf "${message}" |
        awk '/^$/ { exit 1 };
             /protocol="application\/pgp-encrypted"/ { exit 0 }'
    then
      printf "${message}"
    else
      printf "${message}" |
        gawk \
          -v pgp='gpg -qaer $LOGNAME' \
          -f "${HOME}/bin/smegmail"
    fi >> "/var/mail/${LOGNAME}"

## sending mail encrypted with smegmail

I mean, you can, but your MUA will likely do a better job. PGP/MIME signatures
are outside the scope of smegmail, and care must be taken to pass correct
recipient option to gpg if you want him to read the thing.

    gawk -v pgp='gpg -qaer Sean' -f ~/bin/smegmail < mail | sendmail -t

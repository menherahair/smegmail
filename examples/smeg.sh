#!/bin/sh
# smeg.sh - smegmail wrapper that could work in .forward on sdf as-is
# things to note regardless:
# - this uses the old `gpg`, not `gpg2` executable
# - this has no idea about your $GNUPG_HOME
# - the gpg will try to encrypt to your LOGNAME, make sure it works
# - this expects ~/bin/smegmail to be smegmail

# the .forward's environment is damn barren, best I could do in posix
# sh to reliably fix it is this
export HOME="${PWD}"
export LOGNAME="${HOME##*/}"
export PATH="/usr/pkg/bin/:${PATH}"

# call smegmail, encrypt to LOGNAME, append to the system spoolfile
gawk \
  -v pgp='gpg -qaer $LOGNAME' \
  -f "${HOME}/bin/smegmail" >> "/var/mail/${LOGNAME}"

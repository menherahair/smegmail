# smegmail

smegma mail

## what?

smegmail is a gawk program that encrypts mail into PGP/MIME format described in
[RFC3156](https://www.rfc-editor.org/rfc/rfc3156).

## why?

I wanted to encrypt everything I get on sdf.org but refuse to pay for arpa to
use procmail

## how?

Parsing the headers, extracting the content, and rebuilding them into PGP/MIME
message is done by gawk.

External command is used to generate PGP encrypted message content - by default
`gpg --quiet --armor --encrypt`. This relies on gnupg's configuration to do
something sane. Another invocation can be specified by passing the `pgp`
variable to gawk through command line flags or SMEG_PGP environment variable.
See [examples](examples/examples.md) for more info.

## bugs

Its UNIX

## license, copying, distributing

see [LICENSE](LICENSE)
